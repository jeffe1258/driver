package com.uleam.facci.drivertesis.activities.cliente;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.florent37.shapeofview.shapes.BubbleView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.uleam.facci.drivertesis.Model.Info;
import com.uleam.facci.drivertesis.Providers.GoogleApiProvider;
import com.uleam.facci.drivertesis.Providers.InfoProvider;
import com.uleam.facci.drivertesis.R;
import com.uleam.facci.drivertesis.includes.MyToolbar;
import com.uleam.facci.drivertesis.utils.DecodePoints;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailRequestActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;

    Integer counter;
    Button inc, dec;
    TextView result;
    TextInputEditText mDescripcion;
    private double mExtraOriginLat;
    private double mExtraOriginLng;
    private double mExtraDestinationLat;
    private double mExtraDestinationLng;
    private String mExtraOrigin;
    private String mExtraDestination;


    private CircleImageView mCircleImageBack;

    private LatLng mOriginLatLng;
    private LatLng mDestinationLatLng;


    private List<LatLng> mPolylineList;
    private PolylineOptions mPolylineOptions;


    private TextView mTextViewOrigin;
    private TextView mTextViewDestination;
    private TextView mTextViewTime;
    private TextView mTextViewPrice;

    private Button mButtonRequest;
    private GoogleApiProvider mGoogleApiProvider;
    private InfoProvider mInfoProvider;
    Double valorinicial;
    Double valorinicial2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_request);
        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);


        counter = 1;

        inc =(Button)findViewById(R.id.tombolinc);
        dec = (Button)findViewById(R.id.tomboldec);
        result = (TextView)findViewById(R.id.Result);
        mDescripcion = (TextInputEditText) findViewById(R.id.descrip);
        result.setText(Integer.toString(counter));
        mCircleImageBack = findViewById(R.id.circleImageBack);
        inc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incremetar();
            }
        });
        dec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decrecer();
            }
        });

        // MyToolbar.show(this, "Información del viaje", true);

        mGoogleApiProvider = new GoogleApiProvider(DetailRequestActivity.this);

        mExtraOriginLat = getIntent().getDoubleExtra("origin_lat", 0);
        mExtraOriginLng = getIntent().getDoubleExtra("origin_lng", 0);
        mExtraDestinationLat = getIntent().getDoubleExtra("destination_lat", 0);
        mExtraDestinationLng = getIntent().getDoubleExtra("destination_lng", 0);
        mExtraOrigin = getIntent().getStringExtra("origin");
        mExtraDestination = getIntent().getStringExtra("destination");


        mOriginLatLng = new LatLng(mExtraOriginLat, mExtraOriginLng);
        mDestinationLatLng = new LatLng(mExtraDestinationLat, mExtraDestinationLng);
        mInfoProvider = new InfoProvider();

        mTextViewOrigin = findViewById(R.id.textViewOrigin);
        mTextViewDestination = findViewById(R.id.textViewDestination);
        mTextViewTime = findViewById(R.id.textViewTime);
        mTextViewPrice = findViewById(R.id.textViewPrice);
        mButtonRequest = findViewById(R.id.btnRequestNow);

        mTextViewOrigin.setText(mExtraOrigin);
        mTextViewDestination.setText(mExtraDestination);



        mButtonRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToRequestDriver();
            }
        });

        mCircleImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        View bottomSheet = findViewById(R.id.bottom_sheet);
        BottomSheetBehavior mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }
            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });


    }

    private void decrecer() {

        if (counter>1){ counter--;}
        result.setText(Integer.toString(counter));

        Calcularpreciopasajeros(valorinicial,valorinicial2,counter);
    }

    private void incremetar() {
        if (counter<=4){
            counter++;
        }

        result.setText(Integer.toString(counter));
   Calcularpreciopasajeros(valorinicial,valorinicial2,counter);
    }


    private void goToRequestDriver() {

        String pasajero = result.getText().toString();
        String precio = mTextViewPrice.getText().toString();
        String descripcion = mDescripcion.getText().toString();

        Intent intent = new Intent(DetailRequestActivity.this, RequestDriverActivity.class);
        intent.putExtra("origin_lat", mOriginLatLng.latitude);
        intent.putExtra("origin_lng", mOriginLatLng.longitude);
        intent.putExtra("origin", mExtraOrigin);
        intent.putExtra("destination", mExtraDestination);
        intent.putExtra("destination_lat", mDestinationLatLng.latitude);
        intent.putExtra("destination_lng", mDestinationLatLng.longitude);
        intent.putExtra("pasajeros",pasajero);
        intent.putExtra("precio",precio);
        intent.putExtra("descripcion", descripcion);

        startActivity(intent);
        finish();
    }

    private void drawRoute() {
        mGoogleApiProvider.getDirections(mOriginLatLng, mDestinationLatLng).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {

                    JSONObject jsonObject = new JSONObject(response.body());
                    JSONArray jsonArray = jsonObject.getJSONArray("routes");
                    JSONObject route = jsonArray.getJSONObject(0);
                    JSONObject polylines = route.getJSONObject("overview_polyline");
                    String points = polylines.getString("points");
                    mPolylineList = DecodePoints.decodePoly(points);
                    mPolylineOptions = new PolylineOptions();
                    mPolylineOptions.color(Color.DKGRAY);
                    mPolylineOptions.width(13f);
                    mPolylineOptions.startCap(new SquareCap());
                    mPolylineOptions.jointType(JointType.ROUND);
                    mPolylineOptions.addAll(mPolylineList);
                    mMap.addPolyline(mPolylineOptions);

                    JSONArray legs =  route.getJSONArray("legs");
                    JSONObject leg = legs.getJSONObject(0);
                    JSONObject distance = leg.getJSONObject("distance");
                    JSONObject duration = leg.getJSONObject("duration");
                    String distanceText = distance.getString("text");
                    String durationText = duration.getString("text");
                    mTextViewTime.setText(durationText+" "+distanceText);
                   // mTextViewDistance.setText(distanceText);
                    String[] distancekm= distanceText.split(" ");
                    double distanciaval=Double.parseDouble(distancekm[0]);
                    String[] mintiempo= distanceText.split(" ");
                    double minval=Double.parseDouble(mintiempo[0]);

                    valorinicial=distanciaval;

                    valorinicial2=minval;
                    Calcularprecio(distanciaval,minval);


                } catch(Exception e) {
                    Log.d("Error", "Error encontrado " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void Calcularprecio(final double distanciaval,final double mintiempo) {

        mInfoProvider.getInfo().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Info info = dataSnapshot.getValue(Info.class);
                    double subtotal = distanciaval * info.getKm();
                    double subtotal2 =mintiempo * info.getMin();
                    double total = subtotal+subtotal2;
                    double mintotal = redondear(total -0.5,2);
                    double maxtotal = redondear(total + 0.5,2);

                    mTextViewPrice.setText("$ "+mintotal+" - "+maxtotal);


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public static double redondear(double d, int c)
    {
        int temp = (int)(d * Math.pow(10 , c));
        return ((double)temp)/Math.pow(10 , c);
    }
    private void Calcularpreciopasajeros(final double distanciaval,final double valormin ,final Integer resultado) {

        mInfoProvider.getInfo().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Info info = dataSnapshot.getValue(Info.class);
                    double subtotal = distanciaval * info.getKm()*resultado;
                    double subtotal2 =valormin * info.getMin()*resultado;
                    double total = subtotal+subtotal2;
                    double mintotal = redondear(total -0.5,2);
                    double maxtotal = redondear(total + 0.5,2);
                    mTextViewPrice.setText("$ "+mintotal+" - "+maxtotal);


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        mMap.addMarker(new MarkerOptions().position(mOriginLatLng).title("Origen").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin)));
        mMap.addMarker(new MarkerOptions().position(mDestinationLatLng).title("Destino").icon(BitmapDescriptorFactory.fromResource(R.drawable.mapa_pin_azul)));

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                new CameraPosition.Builder()
                        .target(mOriginLatLng)
                        .zoom(12f)
                        .build()
        ));
    drawRoute();
    }
}
