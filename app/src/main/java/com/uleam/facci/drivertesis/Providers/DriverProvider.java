package com.uleam.facci.drivertesis.Providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uleam.facci.drivertesis.Model.Conductor;

import java.util.HashMap;
import java.util.Map;

public class DriverProvider {

    DatabaseReference mDatabase;

    public DriverProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Conductores");
    }

    public Task<Void> create(Conductor driver) {
        return mDatabase.child(driver.getId()).setValue(driver);
    }


    public DatabaseReference getDriver(String idDriver) {
        return mDatabase.child(idDriver);
    }

    public Task<Void> update(Conductor driver) {
        Map<String, Object> map = new HashMap<>();
        map.put("nombre", driver.getNombre());
        map.put("image", driver.getImage());
        map.put("marca", driver.getMarca());
        map.put("placa", driver.getPlaca());
        map.put("phone", driver.getPhone());
        return mDatabase.child(driver.getId()).updateChildren(map);
    }
    public Task<Void> update2w(Conductor driver) {
        Map<String, Object> map = new HashMap<>();
        map.put("nombre", driver.getNombre());
        map.put("marca", driver.getMarca());
        map.put("placa", driver.getPlaca());
        map.put("phone", driver.getPhone());
        return mDatabase.child(driver.getId()).updateChildren(map);
    }

}
