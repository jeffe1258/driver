package com.uleam.facci.drivertesis.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.airbnb.lottie.LottieAnimationView;
import com.google.firebase.auth.FirebaseAuth;
import com.uleam.facci.drivertesis.R;
import com.uleam.facci.drivertesis.activities.cliente.MapClierActivity;
import com.uleam.facci.drivertesis.activities.conductor.MapDriverActivity;

import org.aviran.cookiebar2.CookieBar;

public class MainActivity extends AppCompatActivity {


    private LottieAnimationView mAnimation;

    Button mButtonConductor,mButtonCliente;
    SharedPreferences mPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPref= getApplicationContext().getSharedPreferences("typeUser",MODE_PRIVATE);
        final SharedPreferences.Editor editor = mPref.edit();
        mButtonConductor = findViewById(R.id.btn_conductor);

        mAnimation = findViewById(R.id.animationnn);
        mAnimation.playAnimation();
        mButtonCliente = findViewById(R.id.btn_cliente);
        mButtonCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString("user","cliente");
                editor.apply();

                SeleccionarAuth();
            }
        });

        mButtonConductor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("user","conductor");
                editor.apply();
                SeleccionarAuth();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (FirebaseAuth.getInstance().getCurrentUser()!= null){
                String user = mPref.getString("user","");
            if (user.equals("cliente")){
                Intent gg = new Intent(MainActivity.this, MapClierActivity.class);
                CookieBar.build(MainActivity.this)
                        //.setTitle("")
                        .setTitleColor(R.color.colorcinco)
                        .setIcon(R.drawable.ic_check)
                        .setIconAnimation(R.animator.spin)
                        .setMessage("Se registro correctamente")
                        .setDuration(5000) // 5 seconds
                        .show();

                //  Toast.makeText(RegisterDriverActivity.this, "El registro se realizo exitosamente", Toast.LENGTH_SHORT).show();
                gg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(gg);
            }else{
                Intent gg = new Intent(MainActivity.this, MapDriverActivity.class);
                CookieBar.build(MainActivity.this)
                        //.setTitle("")
                        .setTitleColor(R.color.colorcinco)
                        .setIcon(R.drawable.ic_check)
                        .setIconAnimation(R.animator.spin)
                        .setMessage("Se registro correctamente")
                        .setDuration(5000) // 5 seconds
                        .show();


                gg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(gg);
            }

        }
    }

    private void SeleccionarAuth() {
        Intent pasar = new Intent(MainActivity.this,SelectOptionAuthActivity.class);
        startActivity(pasar);
    }
}
