package com.uleam.facci.drivertesis.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.uleam.facci.drivertesis.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class SplashActivity extends AppCompatActivity {

    private LottieAnimationView mAnimation;
    //Variables
    private static int SPLASH_SCREEN = 12000;
    Animation topAnim, bottomAnim;
    CircleImageView logo;
    TextView desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // enlevons la bare en haut
        // enlevons la bare en haut
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        mAnimation = findViewById(R.id.animatioo);

        mAnimation.playAnimation();
        //Animations
        //topAnim = AnimationUtils.loadAnimation(this,R.anim.top_animation);
        bottomAnim = AnimationUtils.loadAnimation(this,R.anim.bottom_animation);

        //Hooks
        //logo = findViewById(R.id.imageView);
        desc = findViewById(R.id.textView);


        //Assignment
        //logo.setAnimation(topAnim);
        desc.setAnimation(bottomAnim);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                overridePendingTransition(0, 0);
                // close this activity
                finish();
            }
        }, SPLASH_SCREEN);
    }
}
