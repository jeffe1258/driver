package com.uleam.facci.drivertesis.activities.cliente;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.uleam.facci.drivertesis.Model.Cliente;
import com.uleam.facci.drivertesis.Providers.AuthProvider;
import com.uleam.facci.drivertesis.Providers.ClientProvider;
import com.uleam.facci.drivertesis.Providers.ImagesProvider;
import com.uleam.facci.drivertesis.R;
import com.uleam.facci.drivertesis.activities.LoginActivity;
import com.uleam.facci.drivertesis.activities.MainActivity;
import com.uleam.facci.drivertesis.activities.conductor.UpdateProfileDriverActivity;
import com.uleam.facci.drivertesis.includes.MyToolbar;
import com.uleam.facci.drivertesis.utils.FileUtil;

import org.aviran.cookiebar2.CookieBar;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class UpdateProfileActivity extends AppCompatActivity {

    private CircleImageView mCircleImage,mCircleImageBack;
    private ImageView mImageViewProfile;
    private Button mButtonUpdate;
    private TextView mTextViewName;
    private TextView mTextViewPhone2;

    private ClientProvider mClientProvider;
    private AuthProvider mAuthProvider;
    private ImagesProvider mImageProvider;


    private File mImageFile;
    private String mImage="";

    private final int GALLERY_REQUEST = 1;
    private ProgressDialog mProgressDialog;
    private String mName;
    private String mPhone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        //MyToolbar.show(this, "Actualizar perfil", true);

       // mImageViewProfile = findViewById(R.id.imageViewProfile);
        mButtonUpdate = findViewById(R.id.btnUpdateProfile);
        mTextViewName = findViewById(R.id.textInputName);
        mCircleImage = findViewById(R.id.imageViewProfile);
        mTextViewPhone2 = findViewById(R.id.input_phone2);
        mCircleImageBack = findViewById(R.id.circleImageBack);
        mClientProvider = new ClientProvider();
        mAuthProvider = new AuthProvider();
        mImageProvider = new ImagesProvider("client_images");
        mProgressDialog = new ProgressDialog(this);

        getClientInfo();

        mCircleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });

        mButtonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile();
            }
        });

        mCircleImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GALLERY_REQUEST );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode== GALLERY_REQUEST && resultCode == RESULT_OK) {
            try {
                mImageFile = FileUtil.from(this, data.getData());
                mCircleImage.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            } catch(Exception e) {
                Log.d("ERROR", "Mensaje: " +e.getMessage());
            }
        }
    }

    private void getClientInfo() {
        mClientProvider.getClient(mAuthProvider.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String name = dataSnapshot.child("nombre").getValue().toString();
                    String cell = dataSnapshot.child("phone").getValue().toString();
                    String image = "";
                    if (dataSnapshot.hasChild("image")) {
                        image = dataSnapshot.child("image").getValue().toString();
                        Picasso.with(UpdateProfileActivity.this).load(image).into(mCircleImage);
                    }
                    mImage= image;
                    mTextViewPhone2.setText(cell);
                    mTextViewName.setText(name);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateProfile() {
        mName = mTextViewName.getText().toString();
        mPhone = mTextViewPhone2.getText().toString();
        //System.out.println(mName+""+mImage);


       // System.out.println("holaaa  "+mImageFile);

        if (!mName.equals("")&& mImageFile!=null&& !mPhone.equals("")) {

            mProgressDialog.setMessage("Espere un momento...");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();

            saveImage();

        }
        else if (mImageFile==null&&!mName.equals("")&&!mPhone.equals("")){

                mProgressDialog.setMessage("Espere un momento...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                Cliente client = new Cliente();
                client.setName(mName);
                client.setId(mAuthProvider.getId());
                client.setPhone(mPhone);
                mClientProvider.update2(client).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        mProgressDialog.dismiss();
                        CookieBar.build(UpdateProfileActivity.this)
                                //.setTitle("")
                                .setTitleColor(R.color.colorcinco)
                                .setIcon(R.drawable.ic_check)
                                .setIconAnimation(R.animator.spin)
                                .setMessage("Su informacion se actualizo correctamente")
                                .setDuration(5000) // 5 seconds
                                .show();
                    }
                });
            }else{


                        mProgressDialog.dismiss();
                        CookieBar.build(UpdateProfileActivity.this)
                                //.setTitle("")
                                .setTitleColor(R.color.colorcuatro)
                                .setIcon(R.drawable.ic_stat)
                                .setIconAnimation(R.animator.spin)
                                .setMessage("Campos vacíos")
                                .setDuration(5000) // 5 seconds
                                .show();


            }

        //    Toast.makeText(this, "Ingresa la imagen y el nombre", Toast.LENGTH_SHORT).show();

    }

    private void saveImage() {
        mImageProvider.saveImage(UpdateProfileActivity.this, mImageFile, mAuthProvider.getId()).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String image = uri.toString();
                            Cliente client = new Cliente();
                            client.setImage(image);
                            client.setName(mName);
                            client.setId(mAuthProvider.getId());
                            client.setPhone(mPhone);
                            mClientProvider.update(client).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    mProgressDialog.dismiss();
                                    CookieBar.build(UpdateProfileActivity.this)
                                            //.setTitle("")
                                            .setTitleColor(R.color.colorcinco)
                                            .setIcon(R.drawable.ic_check)
                                            .setIconAnimation(R.animator.spin)
                                            .setMessage("Su informacion se actualizo correctamente")
                                            .setDuration(5000) // 5 seconds
                                            .show();
                                    // Toast.makeText(UpdateProfileActivity.this, "Su informacion se actualizo correctamente", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }
                else {
                    CookieBar.build(UpdateProfileActivity.this)
                            //.setTitle("")
                            .setTitleColor(R.color.colorcuatro)
                            .setIcon(R.drawable.ic_stat)
                            .setIconAnimation(R.animator.spin)
                            .setMessage("Hubo un error al subir la imagen")
                            .setDuration(5000) // 5 seconds
                            .show();
//                    Toast.makeText(UpdateProfileActivity.this, "Hubo un error al subir la imagen", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
