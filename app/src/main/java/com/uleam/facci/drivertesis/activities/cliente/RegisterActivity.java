package com.uleam.facci.drivertesis.activities.cliente;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.uleam.facci.drivertesis.Model.Cliente;
import com.uleam.facci.drivertesis.Providers.AuthProvider;
import com.uleam.facci.drivertesis.Providers.ClientProvider;
import com.uleam.facci.drivertesis.R;
import com.uleam.facci.drivertesis.activities.conductor.MapDriverActivity;
import com.uleam.facci.drivertesis.activities.conductor.MapDriverBookingActivity;
import com.uleam.facci.drivertesis.activities.conductor.RegisterDriverActivity;
import com.uleam.facci.drivertesis.includes.MyToolbar;

import org.aviran.cookiebar2.CookieBar;

import dmax.dialog.SpotsDialog;

public class RegisterActivity extends AppCompatActivity {
    SharedPreferences mPref;
    Button mButtonregister;
    TextInputEditText mNombre;
    TextInputEditText mCorreo;
    TextInputEditText mPass;
    TextInputEditText mTel;
    AuthProvider mAutProvider;
    ClientProvider mClientProvider;
    AlertDialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAutProvider = new AuthProvider();
        mClientProvider = new ClientProvider();
        MyToolbar.show(this,"Registro de usuario",true);
        mButtonregister = findViewById(R.id.btn_registrarr);
        mNombre = findViewById(R.id.input_nombre);
        mCorreo = findViewById(R.id.input_correoo);
        mPass = findViewById(R.id.input_password2);
        mTel = findViewById(R.id.input_tele);
        mDialog = new SpotsDialog.Builder().setContext(RegisterActivity.this).setMessage("Espere un momento ").build();
        mButtonregister.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            click_registro();
        }
    });
    }

     void click_registro() {

    final String nombre = mNombre.getText().toString();
    final String email = mCorreo.getText().toString();
    final String pass = mPass.getText().toString();
    final String tel = mTel.getText().toString();
    if (!nombre.isEmpty()&& !email.isEmpty()&& !pass.isEmpty()&&!tel.isEmpty()){
     if (pass.length()>=6){
         mDialog.show();
        register(nombre,email,pass,tel);


     }else{
         CookieBar.build(RegisterActivity.this)
                 //.setTitle("")
                 .setTitleColor(R.color.colorcuatro)
                 .setIcon(R.drawable.ic_stat)
                 .setIconAnimation(R.animator.spin)
                 .setMessage("La contraseña debe tener mínimo 6 caracteres")
                 .setDuration(5000) // 5 seconds
                 .show();
         //Toast.makeText(this, "La contraseña debe tener mínimo 6 caracteres", Toast.LENGTH_SHORT).show();
     }
    }else{
        CookieBar.build(RegisterActivity.this)
                //.setTitle("")
                .setTitleColor(R.color.colorcuatro)
                .setIcon(R.drawable.ic_stat)
                .setIconAnimation(R.animator.spin)
                .setMessage("Todos los campos son obligatorios ")
                .setDuration(5000) // 5 seconds
                .show();
       // Toast.makeText(this, "Todos los campos son obligatorios ", Toast.LENGTH_SHORT).show();
    }

    }

     void register( final String nombre,final String email, String pass,final String tel) {
         mAutProvider.register(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
             @Override
             public void onComplete(@NonNull Task<AuthResult> task) {
                 mDialog.hide();
                 if (task.isSuccessful()){
                     String id= FirebaseAuth.getInstance().getCurrentUser().getUid();
                     Cliente cliente = new Cliente(id,nombre,email,tel);
                    // guardar_usuario(id,nombre,emaill);
                        create(cliente);
                 }else{
                     CookieBar.build(RegisterActivity.this)
                             //.setTitle("")
                             .setTitleColor(R.color.colorcuatro)
                             .setIcon(R.drawable.ic_stat)
                             .setIconAnimation(R.animator.spin)
                             .setMessage("No se pudo registrar")
                             .setDuration(5000) // 5 seconds
                             .show();
                   //  Toast.makeText(RegisterActivity.this, "No se pudo registrar", Toast.LENGTH_SHORT).show();
                 }
             }
         });

     }

    void create(Cliente cliente){
        mClientProvider.create(cliente).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Intent gg = new Intent(RegisterActivity.this, MapClierActivity.class);
                    CookieBar.build(RegisterActivity.this)
                            //.setTitle("")
                            .setTitleColor(R.color.colorcinco)
                            .setIcon(R.drawable.ic_check)
                            .setIconAnimation(R.animator.spin)
                            .setMessage("El registro se realizo exitosamente")
                            .setDuration(5000) // 5 seconds
                            .show();
                    // Toast.makeText(RegisterDriverActivity.this, "El registro se realizo exitosamente", Toast.LENGTH_SHORT).show();
                    gg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(gg);
                }else{
                    CookieBar.build(RegisterActivity.this)
                            //.setTitle("")
                            .setTitleColor(R.color.colorcuatro)
                            .setIcon(R.drawable.ic_stat)
                            .setIconAnimation(R.animator.spin)
                            .setMessage("No se pudo registrar cliente")
                            .setDuration(5000) // 5 seconds
                            .show();
                 //   Toast.makeText(RegisterActivity.this, "No se pudo registrar cliente", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


/*
    void guardar_usuario(String id,String nombre,String clave) {

         mPref= getApplicationContext().getSharedPreferences("typeUser",MODE_PRIVATE);
         String SelectedUser = mPref.getString("user","");
         Usuarios user = new Usuarios();
         user.setNombre(nombre);
         user.setEmail(clave);
         if (SelectedUser.equals("conductor")){

            mDatabase.child("Usuarios").child("Conductores").child(id).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(RegisterActivity.this, "Registro exitoso", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(RegisterActivity.this, "Falló el registro", Toast.LENGTH_SHORT).show();
                }
                }
            });
         }else if (SelectedUser.equals("cliente")){

             mDatabase.child("Usuarios").child("Clientes").child(id).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                 @Override
                 public void onComplete(@NonNull Task<Void> task) {
                     if (task.isSuccessful()){
                         Toast.makeText(RegisterActivity.this, "Registro exitoso", Toast.LENGTH_SHORT).show();
                     }else{
                         Toast.makeText(RegisterActivity.this, "Falló el registro", Toast.LENGTH_SHORT).show();
                     }
                 }
             });
         }
    }*/
}
