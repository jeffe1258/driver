package com.uleam.facci.drivertesis.activities.conductor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.uleam.facci.drivertesis.Model.Conductor;
import com.uleam.facci.drivertesis.Providers.AuthProvider;
import com.uleam.facci.drivertesis.Providers.DriverProvider;
import com.uleam.facci.drivertesis.Providers.ImagesProvider;
import com.uleam.facci.drivertesis.R;
import com.uleam.facci.drivertesis.activities.MainActivity;
import com.uleam.facci.drivertesis.activities.cliente.MapClierActivity;
import com.uleam.facci.drivertesis.activities.cliente.UpdateProfileActivity;
import com.uleam.facci.drivertesis.includes.MyToolbar;

import org.aviran.cookiebar2.CookieBar;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class UpdateProfileDriverActivity extends AppCompatActivity {


    private ImageView mImageViewProfile;
    private Button mButtonUpdate;
    private TextView mTextViewName;
    private TextView mTextViewBrandVehicle;

    private TextView mTextViewPhone;
    private TextView mTextViewPlateVehicle;

    private DriverProvider mDriverProvider;
    private AuthProvider mAuthProvider;
    private ImagesProvider mImageProvider;

    private CircleImageView mCircleImage,mCircleImageBack;
    private File mImageFile;
    private String mImage;

    private final int GALLERY_REQUEST = 1;
    private ProgressDialog mProgressDialog;
    private String mName;
    private String mVehicleBrand;
    private String mPhone;
    private String mVehiclePlate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile_driver);

       // MyToolbar.show(this, "Actualizar perfil", true);

        mImageViewProfile = findViewById(R.id.imageViewProfile);
        mButtonUpdate = findViewById(R.id.btnUpdateProfile);
        mTextViewName = findViewById(R.id.textInputName);
        mTextViewPhone = findViewById(R.id.input_phone);
        mTextViewBrandVehicle = findViewById(R.id.textInputVehicleBrand);
        mTextViewPlateVehicle = findViewById(R.id.textInputVehiclePlate);

        mDriverProvider = new DriverProvider();
        mAuthProvider = new AuthProvider();
        mImageProvider = new ImagesProvider("driver_images");

        mProgressDialog = new ProgressDialog(this);

        mCircleImage = findViewById(R.id.imageViewProfile);

        mCircleImageBack = findViewById(R.id.circleImageBack);
        mCircleImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getDriverInfo();

        mCircleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });

        mButtonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile();
            }
        });

    }

    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GALLERY_REQUEST );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode== GALLERY_REQUEST && resultCode == RESULT_OK) {
            try {
                mImageFile = com.uleam.facci.drivertesis.utils.FileUtil.from(this, data.getData());
                mCircleImage.setImageBitmap(BitmapFactory.decodeFile(mImageFile.getAbsolutePath()));
            } catch(Exception e) {
                Log.d("ERROR", "Mensaje: " +e.getMessage());
            }
        }
    }

    private void getDriverInfo() {
        mDriverProvider.getDriver(mAuthProvider.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String name = dataSnapshot.child("nombre").getValue().toString();
                    String vehicleBrand = dataSnapshot.child("marca").getValue().toString();
                    String vehiclePlate = dataSnapshot.child("placa").getValue().toString();

                    String cell = dataSnapshot.child("phone").getValue().toString();
                    String image = "";
                    if (dataSnapshot.hasChild("image")) {

                        image = dataSnapshot.child("image").getValue().toString();

                            Picasso.with(UpdateProfileDriverActivity.this).load(image).into(mCircleImage);


                        }

                    mImage= image;
                    mTextViewName.setText(name);
                    mTextViewBrandVehicle.setText(vehicleBrand);
                    mTextViewPlateVehicle.setText(vehiclePlate);
                    mTextViewPhone.setText(cell);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateProfile() {
        mName = mTextViewName.getText().toString();
        mVehicleBrand = mTextViewBrandVehicle.getText().toString();
        mVehiclePlate = mTextViewPlateVehicle.getText().toString();
        mPhone = mTextViewPhone.getText().toString();
        if (!mName.equals("") && mImageFile != null && !mVehicleBrand.equals("") && !mVehiclePlate.equals("")&&!mPhone.equals("")) {
            mProgressDialog.setMessage("Espere un momento...");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();

            saveImage();

        }
        else if (!mName.equals("") && mImageFile == null && !mVehicleBrand.equals("") && !mVehiclePlate.equals("")&&!mPhone.equals("")){

                mProgressDialog.setMessage("Espere un momento...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                Conductor driver = new Conductor();
                driver.setNombre(mName);
                driver.setId(mAuthProvider.getId());
                driver.setMarca(mVehicleBrand);
                driver.setPlaca(mVehiclePlate);
                driver.setPhone(mPhone);
                mDriverProvider.update2w(driver).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mProgressDialog.dismiss();

                        CookieBar.build(UpdateProfileDriverActivity.this)
                                //.setTitle("")
                                .setTitleColor(R.color.colorcinco)
                                .setIcon(R.drawable.ic_check)
                                .setIconAnimation(R.animator.spin)
                                .setMessage("Su informacion se actualizo correctamente")
                                .setDuration(5000) // 5 seconds
                                .show();

                    }
                });

            }else{


                        CookieBar.build(UpdateProfileDriverActivity.this)
                                //.setTitle("")
                                .setTitleColor(R.color.colorcuatro)
                                .setIcon(R.drawable.ic_stat)
                                .setIconAnimation(R.animator.spin)
                                .setMessage("Campos vacíos")
                                .setDuration(5000) // 5 seconds
                                .show();

                    }


            }



            //Toast.makeText(this, "Ingresa la imagen y el nombre", Toast.LENGTH_SHORT).show();



    private void saveImage() {
        mImageProvider.saveImage(UpdateProfileDriverActivity.this, mImageFile, mAuthProvider.getId()).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    mImageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String image = uri.toString();
                            Conductor driver = new Conductor();
                            driver.setImage(image);
                            driver.setNombre(mName);
                            driver.setId(mAuthProvider.getId());
                            driver.setMarca(mVehicleBrand);
                            driver.setPlaca(mVehiclePlate);
                            driver.setPhone(mPhone);
                            mDriverProvider.update(driver).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    mProgressDialog.dismiss();

                                    CookieBar.build(UpdateProfileDriverActivity.this)
                                            //.setTitle("")
                                            .setTitleColor(R.color.colorcinco)
                                            .setIcon(R.drawable.ic_check)
                                            .setIconAnimation(R.animator.spin)
                                            .setMessage("Su informacion se actualizo correctamente")
                                            .setDuration(5000) // 5 seconds
                                            .show();

                                }
                            });
                        }
                    });
                }
                else {
                    CookieBar.build(UpdateProfileDriverActivity.this)
                            //.setTitle("")
                            .setTitleColor(R.color.colorcuatro)
                            .setIcon(R.drawable.ic_stat)
                            .setIconAnimation(R.animator.spin)
                            .setMessage("Hubo un error al subir la imagen")
                            .setDuration(5000) // 5 seconds
                            .show();
                  //  Toast.makeText(UpdateProfileDriverActivity.this, "Hubo un error al subir la imagen", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
