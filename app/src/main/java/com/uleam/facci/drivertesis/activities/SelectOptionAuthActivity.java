package com.uleam.facci.drivertesis.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.uleam.facci.drivertesis.R;
import com.uleam.facci.drivertesis.activities.cliente.RegisterActivity;
import com.uleam.facci.drivertesis.activities.conductor.RegisterDriverActivity;
import com.uleam.facci.drivertesis.includes.MyToolbar;

public class SelectOptionAuthActivity extends AppCompatActivity {
    Button mButtonGoToLogin;
    Button mButtonGoToRegister;
    SharedPreferences mPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPref = getApplicationContext().getSharedPreferences("typeUser",MODE_PRIVATE);
        setContentView(R.layout.activity_select_option_auth);
        MyToolbar.show(this, "Seleccionar opcion", true);

        mButtonGoToRegister = findViewById(R.id.btn_registro);
        mButtonGoToLogin = findViewById(R.id.btn_tengocuenta);

        mButtonGoToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginGo();
            }
        });
        mButtonGoToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterGo();
            }
        });
    }

    public  void loginGo (){
    Intent jj = new Intent(SelectOptionAuthActivity.this,LoginActivity.class);
    startActivity(jj);


    }
    public void RegisterGo(){
        String typeuser= mPref.getString("user","");
        if (typeuser.equals("cliente")){
            Intent aa = new Intent(SelectOptionAuthActivity.this, RegisterActivity.class);
            startActivity(aa);

        }else{
            Intent dd = new Intent(SelectOptionAuthActivity.this, RegisterDriverActivity.class);
            startActivity(dd);

        }

    }
}
