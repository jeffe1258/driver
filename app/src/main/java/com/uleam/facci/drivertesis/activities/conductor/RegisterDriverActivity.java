package com.uleam.facci.drivertesis.activities.conductor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.uleam.facci.drivertesis.Model.Cliente;
import com.uleam.facci.drivertesis.Model.Conductor;
import com.uleam.facci.drivertesis.Providers.AuthProvider;
import com.uleam.facci.drivertesis.Providers.ClientProvider;
import com.uleam.facci.drivertesis.Providers.DriverProvider;
import com.uleam.facci.drivertesis.R;
import com.uleam.facci.drivertesis.activities.cliente.MapClierActivity;
import com.uleam.facci.drivertesis.activities.cliente.RegisterActivity;
import com.uleam.facci.drivertesis.activities.cliente.RequestDriverActivity;
import com.uleam.facci.drivertesis.includes.MyToolbar;

import org.aviran.cookiebar2.CookieBar;

import dmax.dialog.SpotsDialog;

public class RegisterDriverActivity extends AppCompatActivity {

    // VIEWS
    Button mButtonRegister;
    TextInputEditText mTextInputEmail;
    TextInputEditText mTextInputName;
    TextInputEditText mTextInputVehicleBrand;
    TextInputEditText mTextInputVehiclePlate;
    TextInputEditText mTextInputPassword;
    TextInputEditText mTextInputPhone;
    AuthProvider mAutProvider;
    DriverProvider mDriveProvider;
    AlertDialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_driver);

        mAutProvider = new AuthProvider();
        mDriveProvider = new DriverProvider();
        MyToolbar.show(this,"Registro de conductor",true);
        mButtonRegister = findViewById(R.id.btn_registrarr);
        mTextInputEmail = findViewById(R.id.input_correoo);
        mTextInputName = findViewById(R.id.input_nombre);
        mTextInputVehicleBrand = findViewById(R.id.input_marca);
        mTextInputVehiclePlate = findViewById(R.id.input_placa);
        mTextInputPassword = findViewById(R.id.input_password2);
        mTextInputPhone = findViewById(R.id.input_telef);

        mDialog = new SpotsDialog.Builder().setContext(RegisterDriverActivity.this).setMessage("Espere un momento ").build();
        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_registro();
            }
        });
    }

    void click_registro() {

        final String nombre = mTextInputName.getText().toString();
        final String email = mTextInputEmail.getText().toString();
        final String marca = mTextInputVehicleBrand.getText().toString();
        final String placa = mTextInputVehiclePlate.getText().toString();
        final String pass = mTextInputPassword.getText().toString();
        final String phone = mTextInputPhone.getText().toString();
        if (!nombre.isEmpty()&& !email.isEmpty()&& !pass.isEmpty()&&!marca.isEmpty()&&!placa.isEmpty()&&!phone.isEmpty()){
            if (pass.length()>=6){
                mDialog.show();
                register(nombre,email,pass,marca,placa,phone);

            }else{
                CookieBar.build(RegisterDriverActivity.this)
                        //.setTitle("")
                        .setTitleColor(R.color.colorcuatro)
                        .setIcon(R.drawable.ic_stat)
                        .setIconAnimation(R.animator.spin)
                        .setMessage("La contraseña debe tener mínimo 6 caracteres")
                        .setDuration(5000) // 5 seconds
                        .show();
              //  Toast.makeText(this, "La contraseña debe tener mínimo 6 caracteres", Toast.LENGTH_SHORT).show();
            }
        }else{

            CookieBar.build(RegisterDriverActivity.this)
                    //.setTitle("")
                    .setTitleColor(R.color.colorcuatro)
                    .setIcon(R.drawable.ic_stat)
                    .setIconAnimation(R.animator.spin)
                    .setMessage("Todos los campos son obligatorios ")
                    .setDuration(5000) // 5 seconds
                    .show();
           // Toast.makeText(this, "Todos los campos son obligatorios ", Toast.LENGTH_SHORT).show();
        }

    }

    void register(final String nombre, final String email, String pass, final String marca, final String placa,final String phone) {
        mAutProvider.register(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                mDialog.hide();
                if (task.isSuccessful()){
                    String id= FirebaseAuth.getInstance().getCurrentUser().getUid();
                    Conductor driver = new Conductor(id,nombre,email,marca,placa,phone);
                    // guardar_usuario(id,nombre,emaill);
                    create(driver);
                }else{
                    CookieBar.build(RegisterDriverActivity.this)
                            //.setTitle("")
                            .setTitleColor(R.color.colorcuatro)
                            .setIcon(R.drawable.ic_stat)
                            .setIconAnimation(R.animator.spin)
                            .setMessage("No se pudo registrar")
                            .setDuration(5000) // 5 seconds
                            .show();
                    //Toast.makeText(RegisterDriverActivity.this, "No se pudo registrar", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    void create(Conductor driver){
        mDriveProvider.create(driver).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){

                        CookieBar.build(RegisterDriverActivity.this)
                                //.setTitle("")
                                .setTitleColor(R.color.colorcuatro)
                                .setIcon(R.drawable.ic_stat)
                                .setIconAnimation(R.animator.spin)
                                .setMessage("El registro de conductor se realizó exitosamente")
                                .setDuration(5000) // 5 seconds
                                .show();
                        Intent gg = new Intent(RegisterDriverActivity.this, MapDriverActivity.class);
                        //  Toast.makeText(RegisterDriverActivity.this, "El registro se realizo exitosamente", Toast.LENGTH_SHORT).show();
                        gg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(gg);
                }else{

                        CookieBar.build(RegisterDriverActivity.this)
                                //.setTitle("")
                                .setTitleColor(R.color.colorcuatro)
                                .setIcon(R.drawable.ic_stat)
                                .setIconAnimation(R.animator.spin)
                                .setMessage("No se pudo registrar conductor")
                                .setDuration(5000) // 5 seconds
                                .show();
                   // Toast.makeText(RegisterDriverActivity.this, "No se pudo registrar conductor", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}
