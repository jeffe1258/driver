package com.uleam.facci.drivertesis.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uleam.facci.drivertesis.R;
import com.uleam.facci.drivertesis.activities.cliente.MapClierActivity;
import com.uleam.facci.drivertesis.activities.conductor.MapDriverActivity;
import com.uleam.facci.drivertesis.activities.conductor.RegisterDriverActivity;
import com.uleam.facci.drivertesis.includes.MyToolbar;

import org.aviran.cookiebar2.CookieBar;
import org.aviran.cookiebar2.OnActionClickListener;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class LoginActivity extends AppCompatActivity {
    SharedPreferences mPref;
    TextInputEditText mTextInputEmail;
    TextInputEditText mTextInputPassword;
    Button mButtonLogin;
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    CoordinatorLayout rl;
    private CircleImageView mCircleImageBack;

    AlertDialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
       rl = (CoordinatorLayout) findViewById(R.id.coordinator) ;
//        MyToolbar.show(this,"Login de usuario",true);
        mTextInputEmail    = findViewById(R.id.input_correo);
        mTextInputPassword = findViewById(R.id.input_pass);
        mButtonLogin       = findViewById(R.id.btn_ingresar);

        mCircleImageBack = findViewById(R.id.circleImageBack);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDialog = new SpotsDialog.Builder().setContext(LoginActivity.this).setMessage("Espere un momento ").build();
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
        mPref = getApplicationContext().getSharedPreferences("typeUser",MODE_PRIVATE);
        mCircleImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void login() {

        String email = mTextInputEmail.getText().toString();
        String password = mTextInputPassword.getText().toString();
        if (!email.isEmpty() && !password.isEmpty()){
            if (password.length()>=6){
                mDialog.show();
            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){
                        String user = mPref.getString("user","");
                            if (user.equals("cliente")){
                                Intent gg = new Intent(LoginActivity.this, MapClierActivity.class);
                                //  Toast.makeText(RegisterDriverActivity.this, "El registro se realizo exitosamente", Toast.LENGTH_SHORT).show();
                                gg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(gg);
                            }else{
                                Intent gg = new Intent(LoginActivity.this, MapDriverActivity.class);
                                //  Toast.makeText(RegisterDriverActivity.this, "El registro se realizo exitosamente", Toast.LENGTH_SHORT).show();
                                gg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(gg);
                        }
                        //Toast.makeText(LoginActivity.this, "El login se realizo correctamente", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        CookieBar.build(LoginActivity.this)
                                //.setTitle("")
                                .setTitleColor(R.color.colorcuatro)
                                .setIcon(R.drawable.ic_stat)
                                .setIconAnimation(R.animator.spin)
                                .setMessage("El  Correo o el password son incorrectos")
                                .setDuration(5000) // 5 seconds
                                .show();

                       // Toast.makeText(LoginActivity.this, "la contraseña o el password son incoreectos", Toast.LENGTH_SHORT).show();
                    }

                    mDialog.dismiss();
                }
            });

            }else{
                CookieBar.build(LoginActivity.this)
                        //.setTitle("")
                        .setTitleColor(R.color.colorcuatro)
                        .setIcon(R.drawable.ic_stat)
                        .setIconAnimation(R.animator.spin)
                        .setMessage("La contraseña debe tener mínimo 6 caracteres")
                        .setDuration(5000) // 5 seconds
                        .show();

                //Toast.makeText(LoginActivity.this, "La contraseña debe tener mínimo 6 caracteres", Toast.LENGTH_SHORT).show();
            }




        }else{

            CookieBar.build(LoginActivity.this)
                    //.setTitle("")
                    .setTitleColor(R.color.colorcuatro)
                    .setIcon(R.drawable.ic_stat)
                    .setIconAnimation(R.animator.spin)
                    .setMessage("Hay campos vacíos, por favor verifíquelos")
                    .setDuration(5000) // 5 seconds
                    .show();
            //Toast.makeText(this, "Verifique los campos vacíos", Toast.LENGTH_SHORT).show();
        }

    }

}
