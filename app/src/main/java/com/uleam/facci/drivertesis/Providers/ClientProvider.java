package com.uleam.facci.drivertesis.Providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uleam.facci.drivertesis.Model.Cliente;

import java.util.HashMap;
import java.util.Map;

public class ClientProvider {

    DatabaseReference mDatabase;

    public ClientProvider() {
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Usuarios").child("Clientes");
    }

    public Task<Void> create(Cliente client) {
        Map<String, Object> map = new HashMap<>();
        map.put("nombre", client.getName());
        map.put("email", client.getEmail());
        map.put("phone", client.getPhone());
        return mDatabase.child(client.getId()).setValue(map);
    }


    public Task<Void> update(Cliente client) {
        Map<String, Object> map = new HashMap<>();
        map.put("image", client.getImage());
        map.put("nombre", client.getName());
        map.put("phone", client.getPhone());
        return mDatabase.child(client.getId()).updateChildren(map);
    }


    public Task<Void> update2(Cliente client) {
        Map<String, Object> map = new HashMap<>();
        map.put("nombre", client.getName());
        map.put("phone", client.getPhone());
        return mDatabase.child(client.getId()).updateChildren(map);
    }


    public DatabaseReference getClient(String idClient) {
        return mDatabase.child(idClient);
    }

}
