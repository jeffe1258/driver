package com.uleam.facci.drivertesis.retrofit;


import com.uleam.facci.drivertesis.Model.FCMBody;
import com.uleam.facci.drivertesis.Model.FCMResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IFCMApi {

    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAApZ7MYl4:APA91bFTBfs3uMcSTcOvDgkdqKdkrenyBHZfoDYr2MHLRN5O8HOR-pZvAxFjuWAu8e2UpBpb6QpZs-AWnLCQyACeBUR4GjVA3LWJ5kpTgdvGqD19AcQYzobjahOPxpJXD4j5UE8n0tmN"
    })
    @POST("fcm/send")
    Call<FCMResponse> send(@Body FCMBody body);

}
