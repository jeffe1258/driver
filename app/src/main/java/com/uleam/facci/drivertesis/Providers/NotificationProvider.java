package com.uleam.facci.drivertesis.Providers;

import com.uleam.facci.drivertesis.Model.FCMBody;
import com.uleam.facci.drivertesis.Model.FCMResponse;
import com.uleam.facci.drivertesis.retrofit.IFCMApi;
import com.uleam.facci.drivertesis.retrofit.RetrofitClient;

import retrofit2.Call;

public class NotificationProvider {

    private String url = "https://fcm.googleapis.com";

    public NotificationProvider() {
    }

    public Call<FCMResponse> sendNotification(FCMBody body) {
        return RetrofitClient.getClientObject(url).create(IFCMApi.class).send(body);
    }
}
